import uuid

from werkzeug.security import generate_password_hash

from src import db


class Pokemon(db.Model):
    __table__ = db.Model.metadata.tables['pokemon']

    # stats = db.relationship('PokemonBaseStats', backref=db.backref('pokemon', uselist=False, lazy='subquery'),
    #                         lazy=True)

    def __repr__(self):
        return f'Pokemon {self.pok_name} #{self.pok_id}'

    def to_dict(self):
        return {
            'id': self.pok_id,
            'pok_name': self.pok_name,
            'pok_height': self.pok_height,
            'pok_weight': self.pok_weight,
            'pok_base_experience': self.pok_base_experience
        }


class PokemonEvolution(db.Model):
    __table__ = db.Model.metadata.tables['pokemon_evolution']

    def __repr__(self):
        return f'evol_id: {self.evol_id}, evolved_species_id: {self.evolved_species_id}'

    def to_dict(self):
        return {
            'evol_id': self.evol_id,
            'evolved_species_id': self.evolved_species_id,
            'evol_minimum_level': self.evol_minimum_level
        }


class PokemonBaseStats(db.Model):
    __table__ = db.Model.metadata.tables['base_stats']

    pokemon = db.relationship('Pokemon', backref=db.backref('stats', uselist=False, lazy='subquery'),
                            lazy=True)

    def __repr__(self):
        return f'{self.pok_id} HP: {self.b_hp}, Attack: {self.b_atk}, Defence: {self.b_def}'

    def to_dict(self):
        return {
            'HP': self.b_hp,
            'ATK': self.b_atk,
            'DEF': self.d_def
        }


users_pokemons = db.Table(
    'users_pokemons',
    db.Column('pok_id', db.Integer, db.ForeignKey('pokemon.pok_id'), primary_key=True),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    extend_existing=True
)


class User(db.Model):
    __tablename__ = 'user'
    __table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(254), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)
    uuid = db.Column(db.String(36), unique=True)
    pokemons = db.relationship('Pokemon', secondary=users_pokemons, lazy=True, backref=db.backref('users', lazy=True))

    def __init__(self, username, email, password, is_admin=False, pokemons=None):
        self.username = username
        self.email = email
        self.password = generate_password_hash(password)
        self.is_admin = is_admin
        self.uuid = str(uuid.uuid4())
        if pokemons:
            self.pokemons = pokemons
        else:
            self.pokemons = []

    def __repr__(self):
        return f'User({self.username}, {self.email}, {self.uuid})'
