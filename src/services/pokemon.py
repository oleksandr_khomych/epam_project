from src.models import Pokemon
from src.services.base_service import Service


class PokemonService(Service):
    @staticmethod
    def get_all_pokemons():
        return Pokemon.query.all()

    @staticmethod
    def get_pokemon_by_id(pok_id):
        return Pokemon.query.filter(Pokemon.pok_id == pok_id).first()

    @staticmethod
    def count_pokemons():
        return Pokemon.query.count()
