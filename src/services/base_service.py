from src import db


class Service:
    @staticmethod
    def add_entity_to_session(entity):
        db.session.add(entity)
        db.session.commit()

    @staticmethod
    def delete_entity_from_session(entity):
        db.session.delete(entity)
        db.session.commit()

    @staticmethod
    def rollback_session():
        db.session.rollback()
