from src.models import User
from src.services.base_service import Service


class UserService(Service):
    @staticmethod
    def get_all_users():
        return User.query.all()

    @staticmethod
    def get_user_by_uuid(uuid):
        return User.query.filter(User.uuid == uuid).first()

    @staticmethod
    def get_user_by_username(username):
        return User.query.filter(User.username == username).first()
