from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from marshmallow_sqlalchemy.fields import Nested

from src.models import Pokemon


class PokemonSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Pokemon
        exclude = ['pok_id']
        include_relationships = True
        load_instance = True

    stats = Nested('BaseStatsSchema', exclude=('pokemon',))
    users = Nested('UserSchema', many=True, exclude=('pokemons',))
