from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from marshmallow_sqlalchemy.fields import Nested

from src.models import PokemonBaseStats


class BaseStatsSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = PokemonBaseStats
        include_relationships = True
        include_fk = True
        load_instance = True

    pokemon = Nested('PokemonSchema', exclude=('pok_id', 'stats'))
