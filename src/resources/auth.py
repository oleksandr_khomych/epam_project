import datetime
from functools import wraps

import jwt
from flask import request, jsonify
from flask_restful import Resource
from jwt.exceptions import DecodeError
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from werkzeug.security import check_password_hash

from src import db, app
from src.schemas.user import UserSchema
from src.services.user import UserService


def token_required(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        token = request.headers.get('X-API-KEY', '')
        if not token:
            return "", 401, {"WWW-Authenticate": "Basic realm='Authentication required'"}
        try:
            uuid = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])['user_id']
        except (KeyError, jwt.ExpiredSignatureError, DecodeError):
            return "", 401, {"WWW-Authenticate": "Basic realm='Authentication required'"}
        user = UserService.get_user_by_uuid(uuid)
        if not user:
            return "", 401, {"WWW-Authenticate": "Basic realm='Authentication required'"}
        return func(self, *args, **kwargs)

    return wrapper


def admin_only(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        token = request.headers.get('X-API-KEY', '')
        uuid = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])['user_id']
        user = UserService.get_user_by_uuid(uuid)
        if not user.is_admin:
            return {'message': "You don't have permissions to do it :("}, 403
        return func(self, *args, **kwargs)

    return wrapper


class AuthRegister(Resource):
    user_schema = UserSchema()

    @token_required
    @admin_only
    def get(self, uuid=None):
        if not uuid:
            return self.user_schema.dump(UserService.get_all_users(), many=True), 200
        user = UserService.get_user_by_uuid(uuid)
        if not user:
            return {'message': 'Not found'}, 404
        return self.user_schema.dump(user), 200

    @token_required
    @admin_only
    def post(self):
        try:
            user = self.user_schema.load(request.json, session=db.session)
        except ValidationError as e:
            return {"message": str(e)}
        try:
            UserService.add_entity_to_session(user)
        except IntegrityError:
            UserService.rollback_session()
            return {"message": "Such user exists"}, 409
        return self.user_schema.dump(user), 201


class AuthLogin(Resource):
    def get(self):
        auth = request.authorization
        if not auth:
            return "No auth info", 401, {"WWW-Authenticate": "Basic realm='Authentication required'"}
        user = UserService.get_user_by_username(auth.get('username', ''))
        if not user or not check_password_hash(user.password, auth.get('password', '')):
            return "", 401, {"WWW-Authenticate": "Basic realm='Authentication required'"}
        token = jwt.encode(
            {
                "user_id": user.uuid,
                "exp": datetime.datetime.now() + datetime.timedelta(hours=1)
            }, app.config['SECRET_KEY']
        )
        return jsonify(
            {
                "token": token
            }
        )


class AuthProfile(Resource):
    user_schema = UserSchema()

    @token_required
    def get(self):
        token = request.headers.get('X-API-KEY', '')
        uuid = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])['user_id']
        user = UserService.get_user_by_uuid(uuid)
        return self.user_schema.dump(user), 201
