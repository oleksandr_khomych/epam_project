from flask import request
from flask_restful import Resource
from marshmallow import ValidationError

from src import db
from src.resources.auth import token_required, admin_only
from src.schemas.base_stats import BaseStatsSchema
from src.schemas.pokemon import PokemonSchema
from src.services.pokemon import PokemonService


class PokemonApi(Resource):
    pokemon_schema = PokemonSchema()
    base_stats_schema = BaseStatsSchema()

    @token_required
    def get(self, pok_id=None):
        if not pok_id:
            return self.pokemon_schema.dump(PokemonService.get_all_pokemons(), many=True), 200
        pokemon = PokemonService.get_pokemon_by_id(pok_id)
        if not pokemon:
            return {'message': 'Not found'}, 404
        return self.pokemon_schema.dump(pokemon), 200

    @token_required
    @admin_only
    def post(self):
        try:
            pokemon = self.pokemon_schema.load(request.json, session=db.session)
        except ValidationError as e:
            return {'message': str(e)}, 400
        PokemonService.add_entity_to_session(pokemon)
        return self.pokemon_schema.dump(pokemon), 201

    @token_required
    def put(self, pok_id=None):
        pokemon = PokemonService.get_pokemon_by_id(pok_id)
        if not pokemon:
            return {'message': 'Not found'}, 404
        try:
            pokemon = self.pokemon_schema.load(request.json, instance=pokemon, session=db.session)
        except ValidationError as e:
            return {'message': str(e)}, 400
        PokemonService.add_entity_to_session(pokemon)
        return self.pokemon_schema.dump(pokemon), 200

    @token_required
    def patch(self, pok_id=None):
        pokemon = PokemonService.get_pokemon_by_id(pok_id)
        if not pokemon:
            return {'message': 'Not found'}, 404
        try:
            pokemon = self.pokemon_schema.load(request.json, instance=pokemon, session=db.session, partial=True)
        except ValidationError as e:
            return {'message': str(e)}, 400
        PokemonService.add_entity_to_session(pokemon)
        return self.pokemon_schema.dump(pokemon), 200

    @token_required
    @admin_only
    def delete(self, pok_id=None):
        pokemon = PokemonService.get_pokemon_by_id(pok_id)
        if not pokemon:
            return {'message': 'Not found'}, 404
        PokemonService.delete_entity_from_session(pokemon)
        return '', 204
