from flask import Flask
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

import config

app = Flask(__name__, template_folder='../templates')
app.config.from_object(config.Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
db.Model.metadata.reflect(db.engine)
db.create_all()
api = Api(app)

from src import routes, models
