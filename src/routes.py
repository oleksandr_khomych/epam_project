from src import api
from src.resources.auth import AuthRegister, AuthLogin, AuthProfile
from src.resources.pokemon import PokemonApi
from src.resources.smoke import Smoke


api.add_resource(Smoke, '/smoke', strict_slashes=False)
api.add_resource(PokemonApi, '/pokemon', '/pokemon/<pok_id>', strict_slashes=False)
api.add_resource(AuthRegister, '/register', strict_slashes=False)
api.add_resource(AuthLogin, '/login', strict_slashes=False)
api.add_resource(AuthProfile, '/profile', strict_slashes=False)
