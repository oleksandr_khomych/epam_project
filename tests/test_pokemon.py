from src import db
from src.schemas.pokemon import PokemonSchema
from src.services.pokemon import PokemonService


class TestPokemon:
    test_id = []
    fake_pokemon = {"pok_name": "New pokemon",
                    "pok_height": 123,
                    "pok_weight": 456,
                    "pok_base_experience": 89
                    }
    pokemon_schema = PokemonSchema()

    def test_create_pokemon(self):
        quantity_before = PokemonService.count_pokemons()
        new_pokemon = self.pokemon_schema.load(self.fake_pokemon, session=db.session)
        PokemonService.add_entity_to_session(new_pokemon)
        self.test_id.append(new_pokemon.pok_id)
        quantity_after = PokemonService.count_pokemons()

        assert quantity_before == quantity_after - 1
        assert new_pokemon.pok_name == 'New pokemon'
        assert new_pokemon.pok_height == 123
        assert new_pokemon.pok_weight == 456
        assert new_pokemon.pok_base_experience == 89

    def test_update_pokemon(self):
        pokemon = PokemonService.get_pokemon_by_id(self.test_id[-1])
        pokemon = self.pokemon_schema.load({"pok_name": "TEST_UPDATE"}, instance=pokemon, session=db.session)
        PokemonService.add_entity_to_session(pokemon)

        assert pokemon.pok_name == 'TEST_UPDATE'
        assert pokemon.pok_height == 123
        assert pokemon.pok_weight == 456
        assert pokemon.pok_base_experience == 89

    def test_delete_pokemon(self):
        quantity_before = PokemonService.count_pokemons()
        pokemon = PokemonService.get_pokemon_by_id(self.test_id[-1])
        PokemonService.delete_entity_from_session(pokemon)
        quantity_after = PokemonService.count_pokemons()

        assert quantity_before == quantity_after + 1
