class Config:
    SQLALCHEMY_DATABASE_URI = 'mysql://root:dbroot@localhost:3306/pokemon_db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'you-will-never-know'
